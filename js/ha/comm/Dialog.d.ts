declare namespace ha.comm {
    class Dialog extends ha.core.BaseComponent {
        private _close;
        private _body;
        dataP: CustomDiv;
        constructor();
        onRender(): void;
        elHtml: HTMLElement;
        close: HTMLElement;
        body: HTMLElement;
    }
    class CustomDiv extends ha.core.BaseComponent {
        constructor();
    }
}
