"use strict";
///<reference path="../../../ha/core/BaseComponent.ts"/>
var ha;
(function (ha) {
    var comm;
    (function (comm) {
        class Dialog extends ha.core.BaseComponent {
            constructor(body) {
                super();
                this._bodyText = '';
                this.body = body;
            }
            onRender() {
                this._bodyEl = this.elHtml.querySelector("div.content p");
                this._elHtml.addEventListener('click', () => {
                    this._elHtml.style.display = 'none';
                });
                console.log(this._bodyEl);
            }
            show() {
                this._elHtml.style.display = 'block';
            }
            hide() {
                this._elHtml.style.display = 'none';
            }
            initTemplate() {
                this.template = `
                    <!-- The Modal -->
                    <div class="ha-comm-dialog">
                    
                        <!-- Modal content -->
                        <div class="content">
                            <span class="close">&times;</span>
                            <p>${this._bodyText}</p>
                        </div>
                    </div>`;
            }
            get body() {
                return this._bodyEl ? this._bodyEl.textContent : this._bodyText;
            }
            set body(value) {
                this._bodyText = value;
                if (this._bodyEl)
                    this._bodyEl.innerHTML = value;
                this.initTemplate();
            }
        }
        comm.Dialog = Dialog;
    })(comm = ha.comm || (ha.comm = {}));
})(ha || (ha = {}));
