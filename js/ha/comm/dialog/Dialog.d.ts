/// <reference path="../../core/BaseComponent.d.ts" />
declare namespace ha.comm {
    class Dialog extends ha.core.BaseComponent {
        private _bodyEl;
        private _bodyText;
        constructor(body?: string);
        onRender(): void;
        show(): void;
        hide(): void;
        initTemplate(): void;
        body: string;
    }
}
