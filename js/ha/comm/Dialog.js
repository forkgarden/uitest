"use strict";
var ha;
(function (ha) {
    var comm;
    (function (comm) {
        class Dialog extends ha.core.BaseComponent {
            constructor() {
                super();
                this.dataP = new CustomDiv();
                this.template = `
                    <!-- The Modal -->
                    <div class="ha-comm-dialog">
                    
                        <!-- Modal content -->
                        <div class="content">
                            <span class="close">&times;</span>
                            <p>Some text in the Modal..</p>
                            <dataP></dataP> 
                        </div>
                     
                    </div>`;
            }
            onRender() {
                this._body = this.elHtml.querySelector("div.modal-content p");
            }
            get elHtml() {
                return this._elHtml;
            }
            set elHtml(value) {
                this._elHtml = value;
            }
            get close() {
                return this._close;
            }
            set close(value) {
                this._close = value;
            }
            get body() {
                return this._body;
            }
            set body(value) {
                this._body = value;
            }
        }
        comm.Dialog = Dialog;
        class CustomDiv extends ha.core.BaseComponent {
            constructor() {
                super();
                this._template = `<div class="custom-div">hallo i'm custom sub component</div>`;
                console.log('custom div constructor');
                console.log(this.template);
            }
        }
    })(comm = ha.comm || (ha.comm = {}));
})(ha || (ha = {}));
