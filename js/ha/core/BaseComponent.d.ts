declare namespace ha.core {
    class BaseComponent implements IComponent {
        protected _template: any;
        protected _renderer: Renderer;
        protected _elHtml: HTMLElement;
        constructor();
        onRender(): void;
        render(parent: HTMLElement): void;
        template: any;
        elHtml: HTMLElement;
    }
}
