declare namespace ha.core {
    class Renderer {
        renderHtml(parent: HTMLElement, component: BaseComponent): void;
    }
    interface IComponent {
        elHtml?: HTMLElement;
        template?: string;
        onRender?(): void;
        render?(parent: HTMLElement): void;
    }
}
