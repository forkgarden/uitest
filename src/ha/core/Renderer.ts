namespace ha.core {
    export class Renderer {

        renderHtml(parent: HTMLElement, component: BaseComponent) {
            let div: HTMLElement = document.createElement('div');
            let el: HTMLElement;

            div.innerHTML = component.template;

            el = div.firstElementChild as HTMLElement;

            component.elHtml = el;
            parent.appendChild(el);

            if (component.onRender) {
                component.onRender()
            }

            let colls: NodeListOf<Element> = el.querySelectorAll('*');

            let key: string;
            for (let i: number = 0; i < colls.length; i++) {
                for (key in component) {
                    if (key.toLowerCase() == colls[i].tagName.toLowerCase()) {

                        let el: HTMLElement = colls[i] as HTMLElement;
                        let comp: BaseComponent = component[key] as BaseComponent;

                        if (comp && comp.render) comp.render(el);
                    }
                }
            }
        }
    }

    export interface IComponent {
        elHtml?: HTMLElement;
        template?: string;
        onRender?(): void;
        render?(parent: HTMLElement): void;
    }

}