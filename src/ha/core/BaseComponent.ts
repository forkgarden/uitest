namespace ha.core {
    export class BaseComponent implements IComponent {
        protected _template;
        protected _renderer: Renderer;
        protected _elHtml: HTMLElement;

        constructor() {
            this._renderer = new Renderer();
        }

        onRender(): void {

        }

        render(parent: HTMLElement): void {
            this._renderer.renderHtml(parent, this);
        }

        public get template() {
            return this._template;
        }

        public set template(value) {
            this._template = value;
        }

        public get elHtml(): HTMLElement {
            return this._elHtml;
        }
        public set elHtml(value: HTMLElement) {
            this._elHtml = value;
        }

    }
}