///<reference path="../../../ha/core/BaseComponent.ts"/>

namespace ha.comm {
    export class Dialog extends ha.core.BaseComponent {
        private _bodyEl: HTMLElement;
        private _bodyText: string = '';

        constructor(body?: string) {
            super();

            this.body = body;
        }

        onRender(): void {
            this._bodyEl = this.elHtml.querySelector("div.content p");
            this._elHtml.addEventListener('click', ()=> {
                this._elHtml.style.display='none'; //TODO: fixme later
            })
            console.log(this._bodyEl);
        }

        show():void {
            this._elHtml.style.display='block';
        }

        hide():void {
            this._elHtml.style.display='none';
        }
 
        initTemplate():void {
            this.template = `
                    <!-- The Modal -->
                    <div class="ha-comm-dialog">
                    
                        <!-- Modal content -->
                        <div class="content">
                            <span class="close">&times;</span>
                            <p>${this._bodyText}</p>
                        </div>
                    </div>`;
        }

        public get body(): string {
            return this._bodyEl?this._bodyEl.textContent : this._bodyText;
        }

        public set body(value: string) {
            this._bodyText = value;
            if (this._bodyEl) this._bodyEl.innerHTML = value;
            this.initTemplate();
        }
    }
}