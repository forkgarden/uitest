"use strict";
var ha;
(function (ha) {
    var core;
    (function (core) {
        class BaseComponent {
            constructor() {
                this._renderer = new core.Renderer();
            }
            onRender() {
            }
            render(parent) {
                this._renderer.renderHtml(parent, this);
            }
            get template() {
                return this._template;
            }
            set template(value) {
                this._template = value;
            }
            get elHtml() {
                return this._elHtml;
            }
            set elHtml(value) {
                this._elHtml = value;
            }
        }
        core.BaseComponent = BaseComponent;
    })(core = ha.core || (ha.core = {}));
})(ha || (ha = {}));
///<reference path="../../../ha/core/BaseComponent.ts"/>
var ha;
(function (ha) {
    var comm;
    (function (comm) {
        class Dialog extends ha.core.BaseComponent {
            constructor(body) {
                super();
                this._bodyText = '';
                this.body = body;
            }
            onRender() {
                this._bodyEl = this.elHtml.querySelector("div.content p");
                this._elHtml.addEventListener('click', () => {
                    this._elHtml.style.display = 'none';
                });
                console.log(this._bodyEl);
            }
            show() {
                this._elHtml.style.display = 'block';
            }
            hide() {
                this._elHtml.style.display = 'none';
            }
            initTemplate() {
                this.template = `
                    <!-- The Modal -->
                    <div class="ha-comm-dialog">
                    
                        <!-- Modal content -->
                        <div class="content">
                            <span class="close">&times;</span>
                            <p>${this._bodyText}</p>
                        </div>
                    </div>`;
            }
            get body() {
                return this._bodyEl ? this._bodyEl.textContent : this._bodyText;
            }
            set body(value) {
                this._bodyText = value;
                if (this._bodyEl)
                    this._bodyEl.innerHTML = value;
                this.initTemplate();
            }
        }
        comm.Dialog = Dialog;
    })(comm = ha.comm || (ha.comm = {}));
})(ha || (ha = {}));
var ha;
(function (ha) {
    var core;
    (function (core) {
        class Renderer {
            renderHtml(parent, component) {
                let div = document.createElement('div');
                let el;
                div.innerHTML = component.template;
                el = div.firstElementChild;
                component.elHtml = el;
                parent.appendChild(el);
                if (component.onRender) {
                    component.onRender();
                }
                let colls = el.querySelectorAll('*');
                let key;
                for (let i = 0; i < colls.length; i++) {
                    for (key in component) {
                        if (key.toLowerCase() == colls[i].tagName.toLowerCase()) {
                            let el = colls[i];
                            let comp = component[key];
                            if (comp && comp.render)
                                comp.render(el);
                        }
                    }
                }
            }
        }
        core.Renderer = Renderer;
    })(core = ha.core || (ha.core = {}));
})(ha || (ha = {}));
