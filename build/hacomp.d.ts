declare namespace ha.core {
    class BaseComponent implements IComponent {
        protected _template: any;
        protected _renderer: Renderer;
        protected _elHtml: HTMLElement;
        constructor();
        onRender(): void;
        render(parent: HTMLElement): void;
        template: any;
        elHtml: HTMLElement;
    }
}
declare namespace ha.comm {
    class Dialog extends ha.core.BaseComponent {
        private _bodyEl;
        private _bodyText;
        constructor(body?: string);
        onRender(): void;
        show(): void;
        hide(): void;
        initTemplate(): void;
        body: string;
    }
}
declare namespace ha.core {
    class Renderer {
        renderHtml(parent: HTMLElement, component: BaseComponent): void;
    }
    interface IComponent {
        elHtml?: HTMLElement;
        template?: string;
        onRender?(): void;
        render?(parent: HTMLElement): void;
    }
}
